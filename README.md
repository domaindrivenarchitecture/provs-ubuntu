# Provs-ubuntu

This repo is part of the [provs framework](https://gitlab.com/domaindrivenarchitecture/provs-docs). 
It provides provs-functions for basic system operations on Ubuntu like
* package installation
* filesystem (create, check, delete files and folders)
* key management (ssh resp. gpg)
* user operations (create)

## Usage

For usage examples it is recommended to have a look at [provs-scripts](https://gitlab.com/domaindrivenarchitecture/provs-scripts) or provs-ubuntu-extensions.
